/*
  Updates a line with latest status information about the service.
  See: http://api.virtualq.io/api/docs#!/standard/PUT_standard_lines_id_update_line_json

  The lineIdentifier could be the line's `id` or its `service_phone_number`

  Example:

    updateLine('mail@example.tld', 'secret', '+4903012345', {
      'service_ewt': 300,
      'service_agents_count': 10,
      'service_average_talk_time': 75,
      'service_waiters_count': 15
    });

  Returns `'OK'` when successful, `false` otherwise.
*/
function updateLine(email, password, lineIdentifier, lineAttributes) {
  var data = lineAttributes;
  data.token = getToken(email, password);

  var url = apiUrl('standard/lines/' + lineIdentifier + '/update_line.json');
  var response = JSON.parse(putRequest(url, data));

  if (typeof response.id != 'undefined') {
    return 'OK';
  } else {
    return false;
  };
};

/*
  Verifies that a customer has waited in the virtaulQ line.
  See: http://api.virtualq.io/api/docs#!/standard/GET_standard_lines_id_verify_waiter_json

  The lineIdentifier could be the line's `id` or its `service_phone_number`. The
  customerPhone should be the callers phone number including a country code

  Example:

    verifyCaller('mail@example.tld', 'secret', '+4903012345', '+4904098765'});

  Returns a hash with waiter details when successful, `false` otherwise.
*/
function verifyCaller(email, password, lineIdentifier, customerPhone) {
  var data = {
    'token': getToken(email, password),
    'user_phone': customerPhone
  };

  var url = apiUrl('standard/lines/' + lineIdentifier + '/verify_waiter.json');
  var response = JSON.parse(getRequest(url, data));

  if (typeof response.id != 'undefined') {
    return response;
  } else {
    return false;
  };
};

/* helper methods */

function apiUrl(path) {
  return 'https://api.virtualq.io/api/v1/' + path;
};

function getToken(email, password) {
  var data = {
    'email': email,
    'password': password
  };

  var url = apiUrl('sessions/login.json');
  var response = JSON.parse(postRequest(url, data));

  if (typeof response.token != 'undefined') {
    return response.token;
  } else {
    throw 'virtualQ: Login failed!';
  };
};

function encodeQueryParameter(data) {
  var parameters = [];
  for (var d in data) {
    parameters.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
  }
  return parameters.join('&');
};

function getRequest(url, data) {
  var request = new XMLHttpRequest();
  request.open('GET', url + '?' + encodeQueryParameter(data), false);
  request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
  request.send(null);
  return request.responseText;
};

function postRequest(url, data) {
  var request = new XMLHttpRequest();
  request.open('POST', url, false);
  request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
  request.send(JSON.stringify(data));
  return request.responseText;
};

function putRequest(url, data) {
  var request = new XMLHttpRequest();
  request.open('PUT', url, false);
  request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
  request.send(JSON.stringify(data));
  return request.responseText;
};
